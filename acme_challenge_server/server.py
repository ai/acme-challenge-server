from acme_challenge_server import letsencrypt
from flask import Flask, abort, request, make_response

app = Flask(__name__)


@app.route('/.well-known/acme-challenge/<token>')
def validate_challenge(token):
    # Figure out the hostname used for this request: 'request.host'
    # automatically picks the value of the X-Forwarded-Host header if
    # present, Host otherwise.
    cn = request.host
    if cn.startswith('www.'):
        cn = cn[4:]

    # Search for the acmeRequest object for this domain. Note that
    # challenges for domain.org and www.domain.org are both stored in
    # the object for domain.org, so find that first.
    task = app.queue.get_task(cn)
    if not task or task.is_new():
        app.logger.error(
            'received validation request for unknown domain %s',
            request.host)
        abort(404)

    state = letsencrypt._State.deserialize(task['acmeState'])
    if request.host not in state.challenges:
        app.logger.error(
            'mismatched domain in request: got %s, expected one of %s',
            request.host, ', '.join(state.challenges.keys()))             
        abort(404)
    challenge = state.challenges[request.host]
    chalb = letsencrypt.supported_challb(challenge.auth)

    # Check the token, or directly the path -- same thing.
    if chalb.path != request.path:
        app.logger.error('bad token for %s: got %s, expected %s',
                         request.host, request.path, chalb.path)
        abort(404)

    app.logger.info('validation request for %s', request.host)
    return make_response(challenge.validation)


def create_app(config={}):
    app.config.from_envvar('APP_CONFIG', silent=True)
    if config:
        app.config.update(config)

    app.queue = letsencrypt.ACMEQueue()
    return app


def main():
    create_app().run()


if __name__ == '__main__':
    main()
