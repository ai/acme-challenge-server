#!/usr/bin/python
#
# Periodic script that runs the acme state machine on all user domains.
#

import ai
import dns.resolver
import itertools
import logging
import optparse
import os
import socket
from acme_challenge_server import letsencrypt

# File to read configuration from.
LETSENCRYPT_CONFIG = os.getenv(
    'LETSENCRYPT_CONFIG',
    '/etc/ai/letsencrypt/acme.conf')

DEFAULT_CONFIG = {
    # Domains that we wish to exclude from certificate generation.
    'EXCLUDE_DOMAINS': frozenset([]),

    # Additional domains that we wish to generate certificates for, and are not in LDAP.
    'INCLUDE_DOMAINS': [
        'autistici.org',
        'inventati.org',
        'onenetbeyond.org',
    ],

    # Request these SANs
    'SAN_DOMAINS': {},
}

resolver = dns.resolver.Resolver(configure=False)
resolver.nameservers = ['8.8.8.8', '8.8.4.4']

_frontend_addrs = frozenset(
    x.address for x in resolver.query('www.autistici.org', 'A'))


def _is_hosted(cn):
    """Return True if a domain is hosted by us.

    This only looks at A records, so we do not generate certificates
    for domains that only host email. This is fine for the moment,
    given that we only support HTTP domain verification.

    """
    try:
        addrs = set(x.address for x in resolver.query(cn, 'A'))
        if addrs.intersection(_frontend_addrs):
            return True
    except Exception as e:
        logging.error('%s: %s', cn, e)
        pass
    return False


def _all_domains(exclude_domains):
    # User domains.
    root = ai.LDAPObj('ou=People,' + ai.config.base_uid)
    for obj in root.search('(&(objectClass=virtualHost)(status=active))'):
        cn = obj['cn']
        if cn not in exclude_domains and _is_hosted(cn):
            yield cn
    # Mail-only public domains.
    root = ai.LDAPObj('ou=Domains,' + ai.config.base_uid)
    for obj in root.search('(&(objectClass=investiciDomain)(acceptMail=true))'):
        cn = obj['cn']
        if cn not in exclude_domains:
            yield cn


def main():
    parser = optparse.OptionParser()
    parser.add_option('--debug', action='store_true', help='Verbose logging')
    parser.add_option('--test', action='store_true', help='Use the test letsencrypt API')
    parser.add_option('--domain', help='Limit to a single domain (for testing)')
    parser.add_option('--force', action='store_true')
    opts, args = parser.parse_args()
    if args:
        parser.error('Too many arguments')

    config = DEFAULT_CONFIG.copy()
    execfile(LETSENCRYPT_CONFIG, {}, config)

    logging.basicConfig(level=logging.DEBUG if opts.debug else logging.INFO)

    q = letsencrypt.ACMEQueue()
    credstore = letsencrypt.ACMECredentialsStore()
    directory_url = letsencrypt.ACME_PROD_DIRECTORY_URL
    if opts.test:
        directory_url = letsencrypt.ACME_TEST_DIRECTORY_URL
    sm = letsencrypt.ACMEStateMachine(credstore, directory_url=directory_url)

    if opts.domain:
        domains = [opts.domain]
    else:
        domains = config['INCLUDE_DOMAINS'] + list(_all_domains(config['EXCLUDE_DOMAINS']))

    for domain, san in config['SAN_DOMAINS'].iteritems():
        if domain not in domains:
            logging.error('SAN domain %s not configured', domain)
        # Does it make sense for SANs to be in the domain list themselves?
        #for s in san:
        #    if s not in domains:
        #        logging.error('SAN domain %s (for %s) not configured', s, domain)
    letsencrypt.run_acme(q, credstore, sm, domains, config['SAN_DOMAINS'], force=opts.force)


if __name__ == '__main__':
    main()
