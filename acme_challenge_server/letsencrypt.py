"""Automatic SSL certificate generation using letsencrypt.

This module takes care of generating SSL certificates for the hosted
user domains, and more. SSL credentials are stored in LDAP (our
"distributed database") and are encrypted with a shared key in order
to preserve confidentiality of offline data storage (backups).

The letsencrypt integration works as follows:

We'd like to support different types of challenges (HTTP- and
DNS-based ones, for instance). This is incompatible with a monolithic
architecture, as we may have to modify the service configuration
(think of creating the necessary DNS record), which has potentially a
high latency. So we thought it would be best to structure the workflow
as an asynchronous process, with two cooperating agents:

* a periodic cron job that runs a finite state machine for each
  domain, and interacts with the letsencrypt API;

* agents that respond to validation requests, either by modifying the
  configuration of our services (DNS) or by some other means (HTTP).

There is a small amount of state (the signed response to the ACME
challenge) that needs to be shared between the two components: in our
case, this state is stored in LDAP as 'acmeRequest' objects.

The asynchronous approach has a number of other advantages: FSMs make
it easy to deal with transient errors and they can run on an arbitrary
server, while the separation between components means that only the
cron job (which can easily run as root) needs to have access to the
letsencrypt private key.

The process of obtaining a certificate for a new domain looks
something like this:

* FSM generates an ACME validation request (challenge)

* the system reconfigures itself to respond to the challenge

* FSM verifies that the challenge can be answered and issues the
  certificate request to letsencrypt

The HTTP validation method, which is the only one currently
implemented, is quite simple, as we don't have to actually reconfigure
our NGINX front-ends: we just forward the well-known ACME validation
URL for all domains to a special challenge-response server that serves
responses straight from LDAP.

All the periodic cron job needs to do is to run a step of each of the
active request FSMs. A secondary "synchronization" cron job, that can
run with a much lower frequency (e.g. daily), looks at the list of
user domains, figure out which ones are missing SSL credentials or
whose credentials are about to expire, and creates new request FSMs
for them.

"""

import cPickle as pickle
import logging
import snappy
import time
import urllib2
import OpenSSL
from datetime import datetime, timedelta
from cryptography import x509
from cryptography.x509.oid import NameOID
from cryptography.fernet import Fernet
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from ldap.dn import escape_dn_chars
from ldap.filter import escape_filter_chars

from acme import client as acme_client
from acme import errors as acme_errors
from acme import challenges
from acme import messages
from acme import jose

from ai.userdb import LDAPObj


log = logging.getLogger(__name__)

BASE_UID = 'ou=ACME,dc=investici,dc=org,o=Anarchy'
ACME_TEST_DIRECTORY_URL = 'https://acme-staging.api.letsencrypt.org/directory'
ACME_PROD_DIRECTORY_URL = 'https://acme-v01.api.letsencrypt.org/directory'
DEFAULT_ACME_ACCOUNT_KEY_FILE = '/etc/ssl-ai/acme/account.key'
DEFAULT_ACME_CREDENTIALS_KEY_FILE = '/etc/ssl-ai/acme/credentials.key'
EXPIRATION_SAFETY_MARGIN_DAYS = 21
CREATION_CUTOFF_DATE = None


def gen_pkey(bits):
    """Generate a new private key.

    Returns:
      The PEM-encoded serialized form of the key (unencrypted).
    """
    key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=bits,
        backend=default_backend())
    return key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption())


def gen_csr(key, names):
    """Generate a CSR using the given private key.

    Args:
      key: the private key, in PEM-encoded format.
      names: list of names for the CSR. The first one will be the
             CN of the final certificate, others will be added as
             SubjectAltNames.

    Returns:
      The raw cryptography CSR object.
    """
    log.info('generating CSR for %s', names)
    # Country and organization will effectively be ignored by
    # letsencrypt and replaced with their own values.
    csr = x509.CertificateSigningRequestBuilder().subject_name(x509.Name([
        x509.NameAttribute(NameOID.COUNTRY_NAME, u'IT'),
        x509.NameAttribute(NameOID.ORGANIZATION_NAME, u'Autistici / Inventati'),
        x509.NameAttribute(NameOID.COMMON_NAME, unicode(names[0])),
    ])).add_extension(
        x509.SubjectAlternativeName([
            x509.DNSName(unicode(x)) for x in names]),
        critical=False,
    ).sign(key, hashes.SHA256(), default_backend())
    return csr


def supported_challb(authorization):
    """Find supported challenge body.

    Returns:
      `acme.messages.ChallengeBody` with `http-01` challenge or
      raise an exception.
    """
    for combo in authorization.body.combinations:
        first_challb = authorization.body.challenges[combo[0]]
        if len(combo) == 1 and isinstance(
                first_challb.chall, challenges.HTTP01):
            return first_challb
    raise Exception('no supported challenges found')


class _Challenge(object):

    def __init__(self, auth, response, validation):
        self.auth = auth
        self.response = response
        self.validation = validation

    def __getstate__(self):
        return {
            'auth': self.auth.to_json(),
            'response': self.response.to_json(),
            'validation': self.validation,
        }
        
    def __setstate__(self, state):
        self.auth = messages.AuthorizationResource.from_json(state['auth'])
        self.response = challenges.ChallengeResponse.from_json(state['response'])
        self.validation = state['validation']


class _State(object):

    def __init__(self, **kwargs):
        for k, v in kwargs.iteritems():
            setattr(self, k, v)

    def serialize(self):
        return snappy.compress(pickle.dumps(self, protocol=-1))

    @classmethod
    def deserialize(cls, data):
        return pickle.loads(snappy.uncompress(data))


class ACMEQueue(object):

    def __init__(self):
        self._root = LDAPObj('ou=Queue,' + BASE_UID)

    def _task_obj(self, cn):
        return LDAPObj('cn=%s,%s' % (
                escape_dn_chars(cn), self._root.dn))

    def add_task(self, cn, initial_state='init', san=None):
        newobj = self._task_obj(cn)
        newobj['objectClass'] = ['acmeRequest', 'top']
        newobj['cn'] = cn
        newobj['status'] = initial_state
        if san is not None:
            newobj['san'] = san
        newobj['creationDate'] = datetime.utcnow().isoformat()
        newobj.commit()

    def del_task(self, cn):
        obj = self._task_obj(cn)
        if not obj.is_new():
            obj.delete()

    def has_task(self, cn):
        return not self._task_obj(cn).is_new()

    def get_task(self, cn):
        # The 'cn' could be a domain name or a SAN. We can look up
        # the cn quickly (assembling its DN), and if that fails, we
        # can search for the 'san' attribute instead.
        t = self._task_obj(cn)
        if t.is_new():
            try:
                t = LDAPObj(self._root.dn).search(
                        '(san=%s)' % escape_filter_chars(cn))[0]
            except IndexError:
                t = None
        return t

    def get_tasks(self):
        return self._root.search('(&(objectClass=acmeRequest)(!(status=done)))')


class ACMECredentialsStore(object):

    def __init__(self, secret_key_file=DEFAULT_ACME_CREDENTIALS_KEY_FILE):
        with open(secret_key_file) as fd:
            secret_key = fd.read().strip()
        self._fernet = Fernet(secret_key)
        self._root = LDAPObj('ou=Credentials,' + BASE_UID)

    def _creds_obj(self, cn):
        return LDAPObj('cn=%s,%s' % (
            escape_dn_chars(cn), self._root.dn))

    def get_credentials(self, cn):
        obj = self._creds_obj(cn)
        if obj.is_new():
            return None
        return obj

    def create_credentials(self, cn, pkey):
        newobj = self._creds_obj(cn)
        newobj['objectClass'] = ['sslCredentials', 'top']
        newobj['cn'] = cn
        newobj['sslKey'] = self.encrypt_key(pkey)
        return newobj

    def encrypt_key(self, pkey):
        return self._fernet.encrypt(pkey)

    def decrypt_key(self, pkey):
        return self._fernet.decrypt(pkey)


def _all_names(ldapobj):
    cn = ldapobj['cn']
    names = [cn]
    san_list = ldapobj.get_list('san')
    if san_list:
        names.extend(san_list)
    # Only add "www" prefix to domain names that have one dot (rough
    # approximation to first-level domains).
    if not cn.startswith('www.') and cn.count('.') < 2:
        names.append('www.' + cn)
    return names


class ACMEStateMachine(object):

    def __init__(self, credstore,
                 directory_url=ACME_TEST_DIRECTORY_URL,
                 account_file=DEFAULT_ACME_ACCOUNT_KEY_FILE):
        self._credstore = credstore

        with open(account_file) as fd:
            key = jose.JWKRSA(key=serialization.load_pem_private_key(
                fd.read(), None, default_backend()))
        self._acme = acme_client.Client(directory_url, key)

        # Try to register the account, in case it is the first invocation.
        try:
            reg = self._acme.register()
            self._acme.agree_to_tos(reg)
        except Exception as e:
            log.error('Account registration failed (already registered?): %s', e)

    def run_all(self, ldapobj):
        """Run all steps in sequence until it's done."""
        new_state = 'done'      # in case of timeout, just drop the task.
        for i in xrange(3):
            new_state = self.run(ldapobj)
            if new_state == 'done':
                break
            time.sleep(3)
        return new_state

    def run(self, ldapobj):
        """Run a step of the state machine on 'ldapobj'.

        Returns:
          The object's current state.
        """
        state = ldapobj['status'] or 'init'
        try:
            new_state = getattr(self, 'state_' + state)(ldapobj)
        except acme_errors.Error as e:
            log.exception('ACME error for %s in state %s, bailing out',
                          ldapobj['cn'], state)
            # ACME errors are 'fatal': reset state, and start over.
            return 'done'
        except Exception as e:
            log.exception('Error for %s in state %s', ldapobj['cn'], state)
            # Permanent error. TODO: find a way to tell. In some cases
            # we should definitely reset the ACME challenge/response
            # protocol.
            return 'done'
        if not new_state:
            # Temporary error - will retry later.
            return state
        if new_state != state:
            log.debug('state change: %s %s -> %s', ldapobj['cn'], state, new_state)
            ldapobj['status'] = new_state
            ldapobj.commit()
        return new_state

    def state_init(self, ldapobj):
        """Generate a new private key and initialize the ACME challenge."""
        pkey = gen_pkey(2048)
        creds = self._credstore.create_credentials(ldapobj['cn'], pkey)
        creds.commit()
        return self.state_request_challenge(ldapobj)
        #return 'request_challenge'

    def state_renew(self, ldapobj):
        """Initialize the renewal process, using the existing private key."""
        return self.state_request_challenge(ldapobj)
        #return 'request_challenge'

    def state_request_challenge(self, ldapobj):
        """Verify ownership of the domain using the ACME protocol."""
        # Save the challenge and the validation data for every alt-domain.
        challenges = {}
        for name in _all_names(ldapobj):
            auth = self._acme.request_domain_challenges(name)
            #new_authzr_uri=self._acme.directory.new_authz should be unnecessary now.

            challb = supported_challb(auth)
            response, validation = challb.response_and_validation(self._acme.key)
            challenges[name] = _Challenge(auth, response, validation.encode())

        ldapobj['acmeState'] = _State(challenges=challenges).serialize()

        # We need to yield here, to allow state to be saved to LDAP
        # and properly propagated, so that the HTTP challenge server
        # can be ready.
        return 'verify'

    def state_verify(self, ldapobj):
        """Verify that we are ready to answer the challenges for this domain."""
        state = _State.deserialize(ldapobj['acmeState'])
        for name in _all_names(ldapobj):
            auth = state.challenges[name].auth
            response = state.challenges[name].response
            challb = supported_challb(auth)

            # Attempt to validate the challenge ourselves (by making
            # an HTTP request to the well-known URL).
            verified = response.simple_verify(
                challb.chall, name, self._acme.key.public_key())
            if not verified:
                logging.warning('%s was not successfully verified by the client', name)
                # Bail out and reset the entire process.
                return 'done'
            logging.info('%s verified successfully', name)
        return self.state_answer_challenge(ldapobj)

    def state_answer_challenge(self, ldapobj):
        name = ldapobj['cn']
        vhosts = _all_names(ldapobj)
        state = _State.deserialize(ldapobj['acmeState'])

        # Answer all the domain-specific challenges.
        authzrs = []
        for hostname in vhosts:
            auth = state.challenges[hostname].auth
            response = state.challenges[hostname].response
            challb = supported_challb(auth)
            log.debug('answering challenge %s with response %s', challb, response)
            self._acme.answer_challenge(challb, response)
            authzrs.append(auth)

        creds = self._credstore.get_credentials(name)
        key = serialization.load_pem_private_key(
            self._credstore.decrypt_key(creds['sslKey']),
            None, default_backend())
        csr = gen_csr(key, vhosts)
        # The CSR needs to be forced into the appropriate format.
        wrapped_csr = jose.util.ComparableX509(
            OpenSSL.crypto.load_certificate_request(
                OpenSSL.crypto.FILETYPE_PEM,
                csr.public_bytes(serialization.Encoding.PEM)))
        crt_response, _ = self._acme.poll_and_request_issuance(
            wrapped_csr, authzrs,
            max_attempts=10)

        # We have the certificate, save it along with the chain cert.
        crt = crt_response.body
        crt_pem = OpenSSL.crypto.dump_certificate(
            OpenSSL.crypto.FILETYPE_PEM, crt)
        chain_crt_pem = _asn1_to_pem(_download(crt_response.cert_chain_uri))
        creds['sslCert'] = ''.join([crt_pem, chain_crt_pem])
        creds['expires'] = _get_expiration_date(crt).isoformat()
        creds.commit()

        return 'done'


def _download(uri):
    """Download a URI and return contents."""
    return urllib2.urlopen(uri).read()


def _asn1_to_pem(data):
    crt = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_ASN1, data)
    return OpenSSL.crypto.dump_certificate(OpenSSL.crypto.FILETYPE_PEM, crt)


def _get_expiration_date(crt):
    return datetime.strptime(crt.get_notAfter()[:14], "%Y%m%d%H%M%S")


def _parse_date(s):
    # ignore fractional seconds.
    return datetime.strptime(s[:19], '%Y-%m-%dT%H:%M:%S')


def _expiration_deadline(d):
    return d - timedelta(EXPIRATION_SAFETY_MARGIN_DAYS, 0, 0)


def run_acme(queue, credstore, state_machine, all_domains, san_domains, now=None, force=False):
    """Run the ACME protocol state machine over pending tasks.

    Performs a single iteration of the state machine for each pending
    task. Completed tasks will be automatically removed from the
    queue.

    This function takes a list of domain names that should have
    credentials, and compares it with the contents of the credentials
    store. Missing domains will be added to the task queue. Domains
    whose credentials need to be renewed will also have tasks created
    for them.

    Args:
      queue: an ACMEQueue object
      credstore: an ACMECredentialsStore object
      state_machine: an ACMEStateMachine object
      all_domains: a list of domain names that should have SSL credentials
      san_domains: a map domain name -> list of SANs to request
      now: current time (for testing)
      force: if True, always renew certificates

    """

    pending_task_domains = set()
    for obj in queue.get_tasks():
        pending_task_domains.add(obj['cn'])
        log.info('recovered task %s from queue', obj['cn'])
        # Note: the choice of 'run_all' here serves to limit our interaction with
        # letsencrypt to 1 domain at a time, if possible.
        if state_machine.run_all(obj) == 'done':
            obj.delete()

    if not now:
        now = datetime.utcnow()

    # Examine all known domains and figure out what needs to be done:
    # new certificate, renewal, or nothing.
    for cn in all_domains:
        san = san_domains.get(cn, None)
        # If there is already a pending task for this domain, do nothing.
        if cn in pending_task_domains:
            continue
        # If the domain has no certificate at all, initialize the
        # whole process.
        creds = credstore.get_credentials(cn)
        if not creds:
            log.info('domain %s has no certificate, initializing...', cn)
            queue.add_task(cn, 'init', san)
        # If the sslCredentials object does not have the sslCert attribute,
        # but it isn't in the queue, re-initialize it.
        elif not creds['sslCert']:
            log.info('domain %s has incomplete setup, reinitializing...', cn)
            queue.add_task(cn, 'init', san)
        else:
            expire_at = _parse_date(creds['expires'])
            created_at = expire_at - timedelta(90, 0, 0)
            deadline = _expiration_deadline(expire_at)
            if deadline < now:
                log.info('the certificate for domain %s is about to expire, renewing...', cn)
                queue.add_task(cn, 'renew', san)
            elif CREATION_CUTOFF_DATE and created_at < CREATION_CUTOFF_DATE:
                log.info('the certificate for domain %s must be forcefully renewed...', cn)
                queue.add_task(cn, 'renew', san)
            elif force:
                log.info('forcing renewal of certificate for domain %s...', cn)
                queue.add_task(cn, 'renew', san)
            else:
                log.info('nothing to do for domain %s (expiration: %s)', cn, expire_at)

