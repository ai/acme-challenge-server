# Entry point for gunicorn and mod_wsgi.

import os
from acme_challenge_server import server

os.environ.setdefault('APP_CONFIG', '/etc/ai/letsencrypt/acme.conf')

app = server.create_app()
