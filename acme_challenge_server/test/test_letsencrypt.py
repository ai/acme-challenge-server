import json
import mock
import os
import shutil
import tempfile
import unittest
from datetime import datetime
from cryptography.fernet import Fernet

from ai import LDAPObj
from acme_challenge_server.test import *
from acme_challenge_server import letsencrypt

from acme import challenges
from acme import messages


class GetExpirationDateTest(unittest.TestCase):

    def test_get_expiration_date(self):
        class FakeCRT(object):
            def get_notAfter(self):
                return "20160210205500blablabla"
        d = letsencrypt._get_expiration_date(FakeCRT())
        self.assertEquals(
            datetime(2016, 2, 10, 20, 55, 0),
            d)


class ChallengeSerializationTest(unittest.TestCase):

    def test_challenge_serialization(self):
        resp = json.loads('{"identifier":{"type":"dns","value":"ngvision.org"},"status":"pending","expires":"2016-01-05T09:48:44.557729927Z","challenges":[{"type":"tls-sni-01","status":"pending","uri":"https://acme-staging.api.letsencrypt.org/acme/challenge/QP94IXbneVt3tQ60ArBXmRT_1MplXYJZr3ywLoIC9QY/927898","token":"MhNvwuuWtV66WQjb7BXER9mOkUPzVAYTZUcWBlfWx_Y"},{"type":"dns-01","status":"pending","uri":"https://acme-staging.api.letsencrypt.org/acme/challenge/QP94IXbneVt3tQ60ArBXmRT_1MplXYJZr3ywLoIC9QY/927899","token":"L8E38o9FjNWqiKWxokqZJbzEXiLVpq1DHrt59uc4XHQ"},{"type":"http-01","status":"pending","uri":"https://acme-staging.api.letsencrypt.org/acme/challenge/QP94IXbneVt3tQ60ArBXmRT_1MplXYJZr3ywLoIC9QY/927900","token":"wfcMSYoPcCfSL4ByrQVnhqNDKrqTHX2e_u5fZpzWN_s"}],"combinations":[[1],[2],[0]]}')
        auth = messages.AuthorizationResource(
            body=messages.Authorization.from_json(resp),
            uri='http://example.com',
            new_cert_uri='http://example.com')
        response = challenges.HTTP01Response(
            key_authorization=u'wfcMSYoPcCfSL4ByrQVnhqNDKrqTHX2e_u5fZpzWN_s.hnKlUdJ9sXTcIoqtiD32Sm7xJDYKSYlitHNSvRdGAPo',
            resource='challenge')

        c = letsencrypt._Challenge(auth, response, u'foo')
        state = letsencrypt._State(challenges={'test': c})
        s = state.serialize()
        state = letsencrypt._State.deserialize(s)
        self.assertTrue(state.challenges)
        self.assertTrue('test' in state.challenges)
        self.assertTrue(isinstance(state.challenges['test'].auth,
                                   messages.AuthorizationResource))


class ACMETestBase(TestBase):

    def setUp(self):
        TestBase.setUp(self)
        self.tmpdir = tempfile.mkdtemp()
        account_file = os.path.join(self.tmpdir, 'account.key')
        with open(account_file, 'w') as fd:
            fd.write(letsencrypt.gen_pkey(2048))
        skey_file = os.path.join(self.tmpdir, 'secret.key')
        with open(skey_file, 'w') as fd:
            fd.write(Fernet.generate_key())
        self.q = letsencrypt.ACMEQueue()
        self.credstore = letsencrypt.ACMECredentialsStore(skey_file)
        self.sm = letsencrypt.ACMEStateMachine(self.credstore, account_file=account_file)

    def tearDown(self):
        shutil.rmtree(self.tmpdir)
        TestBase.tearDown(self)


class CredentialsStoreTest(ACMETestBase):

    LDIFS = ['acme_base.ldif', 'acme_credentials.ldif']

    def test_get_credentials(self):
        obj = self.credstore.get_credentials('testcreds.org')
        self.assertTrue(obj)

    def test_create_and_get_credentials(self):
        obj = self.credstore.create_credentials('example.com', 'foobar')
        obj.commit()

        obj = self.credstore.get_credentials('example.com')
        self.assertTrue(obj)
        self.assertEquals('example.com', obj['cn'])

    def test_encrypted_credentials(self):
        pkey = 'foobar'
        obj = self.credstore.create_credentials('example2.com', pkey)

        # Verify that the key is not stored as cleartext.
        self.assertFalse(pkey in obj['sslKey'])

        # Verify that we can read the encrypted key successfully.
        self.assertEquals(pkey, self.credstore.decrypt_key(obj['sslKey']))


class QueueTest(ACMETestBase):

    LDIFS = ['acme_base.ldif']

    def test_add_task(self):
        d = 'example3.com'
        self.assertFalse(self.q.has_task(d))
        self.q.add_task(d)
        self.assertTrue(self.q.has_task(d))

    def test_get_tasks(self):
        d = 'example4.com'
        self.q.add_task(d)
        tasks = list(self.q.get_tasks())
        # There's probably other tasks, too (from other tests)...
        self.assertTrue(len(tasks) > 0)
        self.assertTrue(d in [x['cn'] for x in tasks])

    def test_delete_task(self):
        d = 'example5.com'
        self.q.add_task(d)
        self.assertTrue(self.q.has_task(d))
        self.q.del_task(d)
        self.assertFalse(self.q.has_task(d))


class RequestTest(ACMETestBase):

    LDIFS = ['acme_base.ldif', 'acme_pending.ldif']

    def test_request_init_and_request_challenges(self):
        obj = LDAPObj('cn=ngvision.org,ou=Queue,ou=ACME,dc=investici,dc=org,o=Anarchy')
        self.assertFalse(obj.is_new())

        # Run the init step.
        self.sm.run(obj)
        self.assertEquals('request_challenge', obj['status'])

        # Test that a credentials object containing a private key was
        # correctly created.
        creds = self.credstore.get_credentials('ngvision.org')
        self.assertTrue(creds)
        self.assertTrue(creds['sslKey'])

        # Try to decode the key. Failure will raise an exception.
        value = self.credstore.decrypt_key(creds['sslKey'])
        self.assertTrue(value)

        # Proceed to next step (request_challenge).
        self.sm.run(obj)
        self.assertEquals('verify', obj['status'])

        # Fetch the stored state and decode it.
        state = letsencrypt._State.deserialize(obj['acmeState'])
        self.assertTrue(state)
        self.assertTrue(state.challenges)
        self.assertEquals(2, len(state.challenges))
        self.assertTrue('ngvision.org' in state.challenges)
        #self.assertEquals({}, state.challenges['ngvision.org'])

        # Next step (verify) - this will FAIL.
        self.sm.run(obj)
        self.assertEquals('verify', obj['status'])


class RunAcmeTest(ACMETestBase):

    LDIFS = ['acme_base.ldif', 'acme_credentials.ldif']

    def test_new_domain(self):
        letsencrypt.run_acme(self.q, self.credstore, self.sm, ['newdomain.org'])
        self.assertTrue(self.q.has_task('newdomain.org'))

    def test_renew(self):
        # Test cert expires on 2016/2/10.
        now = datetime(2016, 2, 8, 0, 0, 0)
        letsencrypt.run_acme(self.q, self.credstore, self.sm, ['testcreds.org'], now=now)

        # The queue should now contain an entry for the test domain,
        # with initial status 'renew'.
        tt = filter(lambda x: x['cn'] == 'testcreds.org', self.q.get_tasks())
        self.assertTrue(tt)
        self.assertEquals('renew', tt[0]['status'])

        # Delete it...
        self.q.del_task('testcreds.org')

    def test_renew_before_deadline_does_nothing(self):
        # A date that is before the deadline.
        now = datetime(2015, 12, 1, 0, 0, 0)
        letsencrypt.run_acme(self.q, self.credstore, self.sm, ['testcreds.org'], now=now)

        # Queue should *not* contain the test domain.
        self.assertFalse(self.q.has_task('testcreds.org'))
