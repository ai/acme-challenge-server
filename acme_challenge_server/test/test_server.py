import json
import mock
import unittest

from acme import challenges
from acme import messages

from acme_challenge_server import letsencrypt
from acme_challenge_server import server


def sample_state():
    # A rather convoluted way of generating a test serialized State,
    # with real acme messages in it.
    resp = json.loads('{"identifier":{"type":"dns","value":"ngvision.org"},"status":"pending","expires":"2016-01-05T09:48:44.557729927Z","challenges":[{"type":"tls-sni-01","status":"pending","uri":"https://acme-staging.api.letsencrypt.org/acme/challenge/QP94IXbneVt3tQ60ArBXmRT_1MplXYJZr3ywLoIC9QY/927898","token":"MhNvwuuWtV66WQjb7BXER9mOkUPzVAYTZUcWBlfWx_Y"},{"type":"dns-01","status":"pending","uri":"https://acme-staging.api.letsencrypt.org/acme/challenge/QP94IXbneVt3tQ60ArBXmRT_1MplXYJZr3ywLoIC9QY/927899","token":"L8E38o9FjNWqiKWxokqZJbzEXiLVpq1DHrt59uc4XHQ"},{"type":"http-01","status":"pending","uri":"https://acme-staging.api.letsencrypt.org/acme/challenge/QP94IXbneVt3tQ60ArBXmRT_1MplXYJZr3ywLoIC9QY/927900","token":"wfcMSYoPcCfSL4ByrQVnhqNDKrqTHX2e_u5fZpzWN_s"}],"combinations":[[1],[2],[0]]}')
    auth = messages.AuthorizationResource(
        body=messages.Authorization.from_json(resp),
        uri='http://example.com',
        new_cert_uri='http://example.com')
    response = challenges.HTTP01Response(
        key_authorization=u'wfcMSYoPcCfSL4ByrQVnhqNDKrqTHX2e_u5fZpzWN_s.hnKlUdJ9sXTcIoqtiD32Sm7xJDYKSYlitHNSvRdGAPo',
        resource='challenge')

    c = letsencrypt._Challenge(auth, response, u'foo')
    state = letsencrypt._State(challenges={'ngvision.org': c})
    return state.serialize()


class MockQueueItem(dict):

    def is_new(self):
        return False


class ACMEChallengeServerTest(unittest.TestCase):

    def setUp(self):
        queue = mock.MagicMock()
        self.queue_p = mock.patch('acme_challenge_server.letsencrypt.ACMEQueue', return_value=queue)
        self.queue_p.start()

        queue.get_task.return_value = MockQueueItem(
            cn='ngvision.org',
            acmeState=sample_state())
        
        app = server.create_app({
            'TESTING': True,
            'DEBUG': True,
        })
        self.app = app.test_client()

    def tearDown(self):
        self.queue_p.stop()

    def test_unknown_token(self):
        resp = self.app.get('/.well-known/acme-challenge/foobar',
                            headers=[('Host', 'ngvision.org')])
        self.assertEquals(404, resp.status_code)
    
    def test_valid_token(self):
        resp = self.app.get('/.well-known/acme-challenge/wfcMSYoPcCfSL4ByrQVnhqNDKrqTHX2e_u5fZpzWN_s',
                            headers=[('Host', 'ngvision.org')])
        self.assertEquals(200, resp.status_code)
        self.assertEquals(u'foo', resp.data)
