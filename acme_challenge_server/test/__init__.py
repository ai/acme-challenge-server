import logging
import os
import shutil
import tempfile
import unittest
from ldap_test import LdapServer

# Monkey-patch the configuration.
import ai
ai.config.domain = 'autistici.org'
ai.config.base_uid = 'dc=investici,dc=org,o=Anarchy'
ai.config.ldap_host = '127.0.0.1'
ai.config.ldap_port = 3999
ai.config.ldap_master_host = '127.0.0.1'
ai.config.ldap_bind_dn = 'cn=manager,o=Anarchy'
ai.config.ldap_bind_pw = 'testpass'


def setUp():
    # Silence the py4j transport debugging messages.
    logging.getLogger('py4j.java_gateway').setLevel(logging.ERROR)


class TestBase(unittest.TestCase):

    LDIFS = []

    @classmethod
    def setup_class(cls):
        ldifs = [os.path.join(os.path.dirname(__file__), 'fixtures', x)
                 for x in ['base.ldif'] + cls.LDIFS]
        cls.server = LdapServer({
            'port': 3999,
            'bind_dn': 'cn=manager,o=Anarchy',
            'password': 'testpass',
            'base': {
                'objectclass': ['organization'],
                'dn': 'o=Anarchy',
                'attributes': {'o': 'Anarchy'},
            },
            'ldifs': ldifs,
        })
        cls.server.start()

    @classmethod
    def teardown_class(cls):
        cls.server.stop()

    def tearDown(self):
        # Disconnect our global LDAP connections: it has a single
        # shared buffer for pending changes, so flushing it prevents
        # cross-test contamination.
        ai.disconnect()
