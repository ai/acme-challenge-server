
acme-challenge-server
=====================

This is a standalone HTTP server that responds to ACME verification challenges
using data taken from the ai.letsencrypt token storage.

**DO NOT MODIFY** - this project is without tests, needlessly complex, and
impossible to modify without breaking stuff. Kill with fire.
