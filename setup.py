#!/usr/bin/python

from setuptools import setup, find_packages

setup(
    name="acme-challenge-server",
    version="0.1",
    description="Server for ACME HTTP challenges",
    author="Autistici/Inventati",
    author_email="info@autistici.org",
    url="https://git.autistici.org/ai/acme-challenge-server",
    setup_requires=[],
    zip_safe=False,
    packages=find_packages(),
    package_data={},
    install_requires=[
        "Flask", "acme==0.20.0", "cryptography", "python-snappy",
    ],
    entry_points={
        "console_scripts": [
            "acme-fsm = acme_challenge_server.scripts.run_fsm:main",
            "acme-dump-certs = acme_challenge_server.scripts.dump_certs:main",
        ],
    },
)

